#Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
#PDX-License-Identifier: MIT-0 (For details, see https://github.com/awsdocs/amazon-rekognition-developer-guide/blob/master/LICENSE-SAMPLECODE.)

# https://docs.aws.amazon.com/rekognition/latest/dg/text-detecting-text-procedure.html

import os
import sys
import boto3

if __name__ == "__main__":

    imageFile = sys.argv[1] # 'input.jpg'
    client=boto3.client('rekognition')

    with open(imageFile, 'rb') as image:
        response = client.detect_text(Image={'Bytes': image.read()})


    textDetections=response['TextDetections']
    print(response)
    print('Detected text')
    for text in textDetections:
            print('Detected text:' + text['DetectedText'])
            print('Confidence: ' + "{:.2f}".format(text['Confidence']) + "%")
            print('Id: {}'.format(text['Id']))
            if 'ParentId' in text:
                print('Parent Id: {}'.format(text['ParentId']))
            print('Type:' + text['Type'])
            print()

    print(" ".join(x['DetectedText'] for x in textDetections))
