import os
import sys
import boto3

def run_aws_ocr(fn):

    client = boto3.client('rekognition')

    with open(fn, 'rb') as image:
        response = client.detect_text(Image={'Bytes': image.read()})

    textDetections = response['TextDetections']
    detected_text = ' '.join(x['DetectedText'] for x in textDetections)

    return detected_text[:len(detected_text)//2]
